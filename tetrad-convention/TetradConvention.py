###############################################
######## CHECK TETRAD CONVENTIONS #############
##############################################

"""

Tetrad choice of each code and model can be spotted computing phases differences between modes in multimode waveforms.


"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d


def UIBMod(xarray, angle):

	""" It is simply a custom modulo function """

	ret = np.zeros(len(xarray))
	i = 0
	for x in xarray:
	    mod = x%angle
    
	    if mod>angle/2:
	        ret[i] = mod - angle
	    else:
	        ret[i] = mod
	    i += 1
	return ret


def PhaseOffsetsTD(hlmlist, modelist, times=None, tetrad=1):

	""" Compute 2*phaselm - m*phase22 and take modulo for all modes. Works only for Time Domain modes.

		You need to pass the hlms and the list of modes employed. Assumes that all modes have the same length.

		If you want to track the times of your waveform for plotting latter you can add
		the times array. Otherwise a simple range array will be created.

		Tetrad should be 1 for the original convention of your waveform, -1 if you want to reverse it to the other convention.
	"""

	phoff = np.arange(len(hlmlist[0])*len(modelist),dtype=np.float).reshape(len(modelist),len(hlmlist[0])) #Initialize the list

	pos22 = modelist.index([2,2])
	h22 = tetrad*hlmlist[pos22]
	arg22 = np.unwrap(np.angle(h22))

	if not isinstance(times,list):
		times = np.arange(len(h22))

	j = 0
	for mode in modelist:
	    l, m = mode
	    hlm = tetrad*hlmlist[j]
	    arglm = np.unwrap(np.angle(hlm))
        
	    ph = (2*arglm - m*arg22)
        
	    phoff[j] = UIBMod(ph,2*np.pi)
	    j+=1

	return times, phoff

def PhaseOffsetsFD(hlmlist, modelist, freqs, tetrad=1):
	""" Compute (2/m)*phaselm(m*freq/2) - phase22(freq) and take modulo for all modes. Works only for Frequency domain modes.

		You need to pass the hlms and the list of modes employed. Assumes that all modes have the same length.

		You need to pass the frequency array in order to scale the frequencies for the different modes.

		Tetrad should be 1 for the original convention of your waveform, -1 if you want to reverse it to the other convention.
	"""

	phoff = np.arange(len(hlmlist[0])*len(modelist),dtype=np.float).reshape(len(modelist),len(hlmlist[0]))
    
	pos22 = modelist.index([2,2])
    
	h22 = tetrad*hlmlist[pos22]
	freqs22 = freqs
	arg22 = np.unwrap(np.angle(h22))
    
	j = 0
	for mode in modelist:
	    l, m = mode
	    hlmInt = interp1d(freqs22,hlmlist[j],bounds_error=False) # Interpolate in order to rescale the frequency
	    hlm = tetrad*hlmInt(m*freqs22/2) # mode at the rescaled frequency
        
	    arglm = np.unwrap(np.angle(hlm))
        
	    phoff[j] = UIBMod(-arg22+(2./m)*arglm,2*np.pi)
	    j+=1
    
	return freqs22, phoff

def PlotPhaseOffsets(timesorfreqs, phoff, modelist, plotlabel = 'Phase offsets.', outpath=None):

	"""

	Pass the output of PhaseOffsets and again the modelist. 

	You can add a custom plotlabel and a path to save the figure.

	"""

	xx = timesorfreqs

	for k in np.arange(len(modelist)):
		if k!=modelist.index([2,2]):
			plt.plot(xx, phoff[k],label='22'+str(modelist[k]))
	plt.legend()
	plt.ylim(-2*np.pi-1,2*np.pi+1)
	plt.yticks([-np.pi/2,-np.pi,-3*np.pi/2,-2*np.pi,0,np.pi/2,np.pi,3*np.pi/2,2*np.pi],
	           [r'$-\dfrac{\pi}{2}$',r'$-\pi$',r'$-\dfrac{3\pi}{2}$',r'$-2\pi$','$0$',r'$\dfrac{\pi}{2}$',r'$\pi$',r'$\dfrac{3\pi}{2}$',r'$2\pi$'])
	plt.title(plotlabel)
	plt.grid(True)
	if not isinstance(outpath,str):
		plt.show()
	else:
		plt.savefig(outpath)
