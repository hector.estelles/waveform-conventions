#!/usr/bin/python

#######################################
######## MODES GENERATION #############
#######################################

""" A list of preconstructed templates to get the modes for some of the current available models.

Warning: Some of the models are under review, so the final way to call the modes could change when merge to master.

"""

import lal
import lalsimulation as lalsim
import pycbc.pnutils as pnutils
import h5py
import numpy as np
from pycbc.waveform import get_td_waveform

def SEOBNRv4HMmodes(Mtot, eta, spin1_z, spin2_z, distance, deltaT, f_start22):

	"""
	Taken from the review page of the model: https://git.ligo.org/waveforms/reviews/SEOBNRv4HM/wikis/home

	If you want NR-like amplitudes, for the distance use:

	distance = mtotal*lal.MRSUN_SI

	"""

	m1, m2 = pnutils.mtotal_eta_to_mass1_mass2(Mtot,eta)

	sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModes(deltaT, m1*lal.MSUN_SI,
                                                              m2*lal.MSUN_SI,f_start22, distance,
                                                              spin1_z, spin2_z,41, 0., 0., 0.,
                                                              0.,0.,0.,0.,0.,1.,1.,
                                                              lal.CreateREAL8Vector(10), 0)

	h55 = sphtseries.mode.data.data #This is h_ 55
	h44 = sphtseries.next.mode.data.data #This is h_ 44
	h21 = sphtseries.next.next.mode.data.data #This is h_ 21
	h33 = sphtseries.next.next.next.mode.data.data #This is h_ 33
	h22 = sphtseries.next.next.next.next.mode.data.data #This is h_ 22
    
	## time array (s)
    
	times = (deltaT/(Mtot*lal.MTSUN_SI))*np.arange(0,len(h22))
    
	return times, [h21, h22, h33, h44, h55]

def LVCNRmodes(filepath, mtotal, distance, modelist):

	"""

	Function to read the modes from the LVCNR catalog simulations. (I think originally from Sebastian, but Cecilio Garcia sent it to me.
																	Some modifications.)

	If you want the original NR amplitudes, for the distance use:

	distance = mtotal*lal.MRSUN_SI/(1E6*lal.PC_SI)

	"""
    
	buffer_factor = 1.                    # this works the best
    
	f = h5py.File(filepath,'r')
    
	params = lal.CreateDict()
	lalsim.SimInspiralWaveformParamsInsertNumRelData(params, filepath)
    
    
	# Spins
	# The NR spins need to be transformed into the LAL frame. There is a function that does that for you:
	#spins = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(fRef,mtotal, filepath)
	spins = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(-1, 1.,filepath)
	s1x = spins[0]
	s1y = spins[1]
	s1z = spins[2]
	s2x = spins[3]
	s2y = spins[4]
	s2z = spins[5]
    
	# Metadata parameters masses:
	m1NR = f.attrs['mass1']
	m2NR = f.attrs['mass2']
    
	f_lowNR = f.attrs['f_lower_at_1MSUN']
	f_low = f_lowNR/mtotal  # this choice generates the whole NR waveforms from the beginning
	# f_low = max (f_low, 10)
	fRef = f_low   #beginning of the waveform
	fStart = fRef

	m1 = m1NR*mtotal
	m2 = m2NR*mtotal
    
	f.close()
    
	# Get the modes from the h5 file using get_td_waveform evaluated at only one mode each time
    
	k = 0

	for mode in modelist:

  		l, m = mode
        
   		modes=lalsim.SimInspiralCreateModeArray()
   		lalsim.SimInspiralModeArrayActivateMode(modes, l, m)
   		lalsim.SimInspiralWaveformParamsInsertModeArray(params, modes)
        
   		Ylm = lal.SpinWeightedSphericalHarmonic(0.34598,0.11234,-2,l,m)

   		dt = 1.0/16000
    
   		sp, sc = get_td_waveform(approximant='NR_hdf5',
                                         numrel_data=filepath,
                                         mass1=m1, mass2=m2,
                                         spin1x=s1x, spin1y=s1y,spin1z=s1z,
                                         spin2x=s2x, spin2y=s2y,spin2z=s2z,
                                         delta_t=dt,f_lower=f_low*buffer_factor, f_ref=fRef,
                                         inclination=0.34598, distance=distance, params=params,
                                         mode_array=[[l,m]], coa_phase=0.11234
                                         )
   		hlm = (sp-1j*sc)/Ylm

   		if k==0:
   			hlmdata = hlm.data
   		else:
   			hlmdata = np.concatenate((hlmdata,hlm.data))

   		k+=1

	times = dt*np.arange(len(hlmdata)/len(modelist))
    
	return times, hlmdata.reshape([len(modelist),len(hlmdata)/len(modelist)])

def PhenomHMmodes(Mtot, eta, s1z, s2z, deltaf, fmin, fmax, modelist):

	"""

	Function to get PhenomHM modes. Originally from PhenomHM review? Cecilio's sent it to me. Some modifications.

	Valid modes: 21, 22, 32, 33, 43, 44

	"""

	m1, m2 = pnutils.mtotal_eta_to_mass1_mass2(Mtot,eta)
    
	freqs = lal.CreateREAL8Sequence(2)
	freqs.data = [fmin, fmax]

	hlms = lalsim.SimIMRPhenomHMGethlmModes(freqs, m1*lal.MSUN_SI, m2*lal.MSUN_SI, s1z, s2z, 0., deltaf, fmin, lal.CreateDict())
    
	k = 0
	for mode in modelist:

   		l, m = mode
   		hlm = lalsim.SphHarmFrequencySeriesGetMode(hlms, l, m)

   		if k==0:
   			hlmdata = hlm.data.data
   		else:
   			hlmdata = np.concatenate((hlmdata,hlm.data.data))
   		k+=1

	flm = np.arange( len(hlmdata)/len(modelist)) * deltaf 

	return flm, hlmdata.reshape([len(modelist),len(hlmdata)/len(modelist)])




